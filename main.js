// Fetch trending genres from TikTok API
fetch('https://api.tiktok.com/trending')
  .then(response => response.json())
  .then(data => {
    const trendsSection = document.getElementById('trends');
    data.genres.forEach(genre => {
      const genreElement = document.createElement('div');
      genreElement.textContent = genre;
      genreElement.className = 'p-5 bg-white rounded shadow cursor-pointer';
      genreElement.onclick = () => generateScript(genre);
      trendsSection.appendChild(genreElement);
    });
  });

function generateScript(genre) {
  // Send request to GPT4 API with prompt template and selected genre
  fetch('https://api.openai.com/v4/engines/davinci-codex/completions', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer YOUR_OPENAI_API_KEY'
    },
    body: JSON.stringify({
      prompt: `Generate funny script for ${genre}`,
      max_tokens: 100
    })
  })
    .then(response => response.json())
    .then(data => {
      const scriptSection = document.getElementById('script');
      scriptSection.textContent = data.choices[0].text;
    });
}
